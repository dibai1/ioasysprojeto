package com.example.dibai.ioasys.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Empresa {

    @SerializedName("id")
    private int id;

    @SerializedName("nome")
    private String nome;

    @SerializedName("sub")
    private String tipo;

    @SerializedName("pais")
    private String pais;

    @SerializedName("foto")
    private String foto;

    public Empresa(String nome, String tipo, String pais, String foto) {
        this.nome = nome;
        this.tipo = tipo;
        this.pais = pais;
        this.foto = foto;
    }

    public Empresa(){
        //Construtor
    }

    //Getter and Setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "Empresa{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", tipo='" + tipo + '\'' +
                ", pais='" + pais + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }

}
