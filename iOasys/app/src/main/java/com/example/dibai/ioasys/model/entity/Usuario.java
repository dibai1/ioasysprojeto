package com.example.dibai.ioasys.model.entity;

import com.google.gson.annotations.SerializedName;

public class Usuario {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    //Construtor

    public Usuario(String email, String password) {
        this.email = email;
        this.password = password;
    }

    //Getter and Setter

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
