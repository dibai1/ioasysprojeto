package com.example.dibai.ioasys.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesHelper {
    private Context context;

    public SharedPreferencesHelper(Context context) {
        this.context = context;
    }

    public void saveToSharedPreferences(String token, String client, String uid) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("accessData", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("TOKEN", token);
        editor.putString("CLIENT", client);
        editor.putString("UID", uid);
        editor.apply();
    }
}