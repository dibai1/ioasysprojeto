package com.example.dibai.ioasys.ui.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.dibai.ioasys.R;

public class DetalheActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView textTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        textTitle = (TextView) findViewById(R.id.textTitle);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        String title = bundle.getString("title");
        textTitle.setText(title);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
