package com.example.dibai.ioasys.ui.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dibai.ioasys.R;
import com.example.dibai.ioasys.model.entity.Empresa;
import com.example.dibai.ioasys.ui.adapters.RecyclerAdapter;
import com.example.dibai.ioasys.ui.listeners.ItemClickListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    RecyclerAdapter adapter;

    TextView textInfo;

    ArrayList<Empresa> empresas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        textInfo = (TextView) findViewById(R.id.textInfo);
        setSupportActionBar(toolbar);

        empresas = new ArrayList<>();

        empresas.add(new Empresa("Empresa1", "Négocio", "Brasil", "foto"));
        empresas.add(new Empresa("Empresa2", "Négocio", "Portugal", "foto"));
        empresas.add(new Empresa("Empresa3", "Négocio", "Portugal", "foto"));


        adapter = new RecyclerAdapter(empresas);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Empresa empresa = empresas.get(position);
                String title = empresa.getNome();

                Intent intent = new Intent(MainActivity.this, DetalheActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("title", title);
                intent.putExtras(bundle);

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_toolbar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setQueryHint(getString(R.string.string_pesquisar));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                recyclerView.setVisibility(View.VISIBLE);
                textInfo.setText("");
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

}
