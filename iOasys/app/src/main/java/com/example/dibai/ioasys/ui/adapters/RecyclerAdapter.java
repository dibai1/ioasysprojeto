package com.example.dibai.ioasys.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dibai.ioasys.R;
import com.example.dibai.ioasys.model.entity.Empresa;
import com.example.dibai.ioasys.ui.listeners.ItemClickListener;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    ArrayList<Empresa> empresas;

    private static ItemClickListener itemClickListener;

    public void setOnItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    public RecyclerAdapter(ArrayList<Empresa> empresas) {
        this.empresas = empresas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Empresa empresa = empresas.get(position);

        //Verificar imgFoto

        viewHolder.txtNome.setText(empresa.getNome());
        viewHolder.txtTipo.setText(empresa.getTipo());
        viewHolder.txtPais.setText(empresa.getPais());

    }


    @Override
    public int getItemCount() {
        return empresas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgFoto;
        TextView txtNome;
        TextView txtTipo;
        TextView txtPais;

        public ViewHolder(View itemView) {
            super(itemView);

            imgFoto = (ImageView) itemView.findViewById(R.id.imgFoto);
            txtNome = (TextView) itemView.findViewById(R.id.textNome);
            txtTipo = (TextView) itemView.findViewById(R.id.textTipo);
            txtPais = (TextView) itemView.findViewById(R.id.textPais);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(itemClickListener != null) {
                itemClickListener.onItemClick(getAdapterPosition());
            }
        }
    }
}
