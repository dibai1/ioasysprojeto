package com.example.dibai.ioasys.ui.listeners;

public interface ItemClickListener {
    void onItemClick(int position);
}